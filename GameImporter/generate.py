from pathlib import Path
import re
import hashlib
import configparser

config = configparser.ConfigParser(strict=False)
config.read("../../Config/DefaultEngine.ini")

hasherMap = {
    "SHA_160": "sha160",
    "SHA_224": "sha224",
    "SHA_256": "sha256",
    "SHA_384": "sha384",
    "SHA_512": "sha512",
    "SHA3_224": "sha3_224",
    "SHA3_256": "sha3_256",
    "SHA3_384": "sha3_384",
    "SHA3_512": "sha3_512",
    "Whirlpool": "whirlpool",
}

assetHasherName = config.get('GameImporter.ContentManager', 'AssetHasher', fallback='SHA3_224')
hashName = hasherMap.get(assetHasherName)

if not hashName:
    hashName = 'sha3_224'

searchPattern = re.compile('GI::ResolveAsset\(TEXT\("(.+)"\)\)')

def addToList(string):
    try:
        slashLoc = string.index('/', 1)
    except ValueError:
        return

    path = string[slashLoc+1:]
    #path = u'textures/common/trigger'
    encoded = path.encode("utf-16be")

    data = encoded
    for i in range(4096):
        digest = hashlib.new(hashName, data)
        data = digest.digest()

    #digest = hashlib.sha224()
    #hash = hashlib.pbkdf2_hmac(digest.name, encoded, b'2020', 4096)

    newPath = string[:slashLoc+1] + digest.hexdigest()

    generated_file.write('\\\nif (StringsEqual(AssetPath, TEXT("' + string + '")))' \
        + '{ return TEXT("' + newPath + '"); }')

    print(string + " -> " + newPath)

def searchInFile(filepath):
    with filepath.open('r') as file:
        for line in file:
            matches = searchPattern.findall(line)
            if(len(matches)):
                addToList(matches[0])

def recursiveSearch(p):
    for child in p.iterdir():
        if(child.is_dir()):
            recursiveSearch(child)
        elif(child.suffix == '.cpp'):
            searchInFile(child)

generated_file = open('../../Intermediate/GameImporterCrypt.generated.h', 'w')

generated_file.write('#pragma once\n')
generated_file.write('#define GENERATED_HASHED_ASSETS_NAMES()')

p = Path('../../Source')

recursiveSearch(p)

generated_file.close()
